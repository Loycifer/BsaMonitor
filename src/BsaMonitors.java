import java.util.Scanner;

public class BsaMonitors {

	public static void main(String[] args) {

		class Course {

			public String name;
			public int maxPoints;
			public double grade;

			public Course(String name, int maxPoints) {
				this.name = name;
				this.maxPoints = maxPoints;
			}

			public void setGrade(double grade) {
				this.grade = grade;
			}

			public int getCredits() {
				if (this.grade >= 5.5) {
					return this.maxPoints;

				} else {
					return 0;
				}
			}

		}

		Course[] courseArray = new Course[7];

		courseArray[0] = new Course("Fasten Your Seatbelts", 12);
		courseArray[1] = new Course("Programming", 3);
		courseArray[2] = new Course("Databases", 3);
		courseArray[3] = new Course("Personal Skills", 2);
		courseArray[4] = new Course("Project Skills", 2);
		courseArray[5] = new Course("Wubba Lubba Dub Dub Theory", 3);
		courseArray[6] = new Course("Being Short", 3);

		Scanner userInput = new Scanner(System.in);

		System.out.println("Voer behaalde cijfers in:");
		int totalCourses = courseArray.length;
		for (int courseNumber = 0; courseNumber < totalCourses; courseNumber++) {
			Course currentCourse = courseArray[courseNumber];
			System.out.print(currentCourse.name + ": ");
			currentCourse.setGrade(userInput.nextFloat());
		}

		System.out.println();

		int receivedCredits = 0;
		int totalCredits = 0;

		for (int courseNumber = 0; courseNumber < totalCourses; courseNumber++) {
			Course currentCourse = courseArray[courseNumber];
			receivedCredits += currentCourse.getCredits();
			totalCredits += currentCourse.maxPoints;
			String tabs = "";
			if (currentCourse.name.length() >= 20) {
				tabs = "\t";
			} else if (currentCourse.name.length() <= 10) {
				tabs = "\t\t\t";
			} else {
				tabs = "\t\t";
			}

			System.out.println("Vak/Project: " + currentCourse.name + tabs + "Cijfer: "
					+ Math.round(currentCourse.grade * 10) / 10.0 + "\tBehaalde Punten: " + currentCourse.getCredits());
		}

		userInput.close();

		System.out.println("\nTotaal behaalde studiepunten: " + receivedCredits + "/" + totalCredits);
		if (receivedCredits / (double) totalCredits < 5 / 6.0) {
			System.out.println("PAS OP: je ligt op schema voor een negatief BSA!");
		}

	}

}

